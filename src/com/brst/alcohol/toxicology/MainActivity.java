package com.brst.alcohol.toxicology;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.brst.alcohol.toxicology.base.BaseContainerFragment;
import com.brst.alcohol.toxicology.base.CasesContainerFragment;
import com.brst.alcohol.toxicology.base.ConsultContainerFragment;
import com.brst.alcohol.toxicology.base.FactContainerFragment;
import com.brst.alcohol.toxicology.base.FeedBackContainerFragment;
import com.brst.alcohol.toxicology.base.HomeContainerFragment;
import com.brst.alcohol.toxicology.base.PlaceHolderContainerFragment;

public class MainActivity extends FragmentActivity {

	private FragmentTabHost mTabHost;

	private static final String TAB_HOME = "Home";
	private static final String TAB_SHARE = "Consult";
	private static final String TAB_FACTS = "Facts";
	private static final String TAB_CASES = "Cases";
	private static final String TAB_FEEDBACK = "Feedback";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(),
				android.R.id.tabcontent);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_HOME),
						R.drawable.new_home_selector),
				HomeContainerFragment.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_SHARE),
						R.drawable.new_share_tab_selector),
				ConsultContainerFragment.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_FACTS),
						R.drawable.new_facts_tab_selector),
				FactContainerFragment.class, null);

		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_CASES),
						R.drawable.new_cases_tab_selector),
				CasesContainerFragment.class, null);
		mTabHost.addTab(
				setIndicator(mTabHost.newTabSpec(TAB_FEEDBACK),
						R.drawable.new_feedback_selector),
				FeedBackContainerFragment.class, null);
	}

	public TabSpec setIndicator(TabSpec spec, int resid) {
		// TODO Auto-generated method stub
		View v = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
		// v.setBackgroundResource(resid);
		ImageView imageButton = (ImageView) v.findViewById(R.id.image_tabs);
		imageButton.setImageResource(resid);
		TextView text = (TextView) v.findViewById(R.id.text_tabs);
		// text.setTextColor(R.drawable.text_color_picker);
		text.setText(spec.getTag());

		return spec.setIndicator(v);
	}

	@Override
	public void onBackPressed() {
		boolean isPopFragment = false;
		String currentTabTag = mTabHost.getCurrentTabTag();

		if (currentTabTag.equals(TAB_HOME)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_HOME)).popFragment();
		} else if (currentTabTag.equals(TAB_SHARE)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_SHARE)).popFragment();
		} else if (currentTabTag.equals(TAB_FACTS)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_FACTS)).popFragment();
		} else if (currentTabTag.equals(TAB_CASES)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_CASES)).popFragment();
		} else if (currentTabTag.equals(TAB_FEEDBACK)) {
			isPopFragment = ((BaseContainerFragment) getSupportFragmentManager()
					.findFragmentByTag(TAB_FEEDBACK)).popFragment();
		}

		if (!isPopFragment) {

			if (mTabHost.getCurrentTab() == 0) {
				finish();
			} else {
				mTabHost.setCurrentTab(0);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onBackButtonClick(View v) {
		onBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		for (Fragment fragment : getSupportFragmentManager().getFragments()) {
			fragment.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
