package com.brst.alcohol.toxicology.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.home.Home;

public class HomeContainerFragment extends BaseContainerFragment {

	private boolean mIsViewInited;
	private static final String TAG = "HomeContainerFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "tab 1 oncreateview");
		return inflater.inflate(R.layout.container_framelayout, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i(TAG, "tab 1 container on activity created");
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		} else {
			getChildFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			initView();
		}

	}	

	private void initView() {
		Log.i(TAG, "tab 1 init view");
		Home home = new Home();
		fragmentCurrent = home.getClass().getSimpleName();
		replaceFragment(home, false);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		currentFragment().onActivityResult(requestCode, resultCode, data);

	}

}
