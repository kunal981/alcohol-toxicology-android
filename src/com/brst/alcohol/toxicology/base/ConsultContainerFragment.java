package com.brst.alcohol.toxicology.base;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.consult.ConsultFragment;
import com.brst.alcohol.toxicology.feedback.FeedbackFragment;

public class ConsultContainerFragment extends BaseContainerFragment {

	private boolean mIsViewInited;
	private static final String TAG = "HomeContainerFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i(TAG, "tab 1 oncreateview");
		return inflater.inflate(R.layout.container_framelayout, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.i(TAG, "tab 1 container on activity created");
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		}
	}

	private void initView() {
		Log.i(TAG, "tab 1 init view");
		ConsultFragment consultFragment = new ConsultFragment();
		fragmentCurrent = consultFragment.getClass().getSimpleName();
		replaceFragment(consultFragment, false);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		currentFragment().onActivityResult(requestCode, resultCode, data);

	}

}
