package com.brst.alcohol.toxicology.modal;

public class CaseRetrogradeProfileModel {

	private String caseName;
	private String bacResult;
	private String incidentTime;
	private String testTime;
	private int mindiff;

	public CaseRetrogradeProfileModel(String caseName, String bacResult,
			String incidentTime, String testTime, int mindiff) {
		super();
		this.caseName = caseName;
		this.bacResult = bacResult;
		this.incidentTime = incidentTime;
		this.testTime = testTime;
		this.mindiff = mindiff;
	}

	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}

	public String getBacResult() {
		return bacResult;
	}

	public void setBacResult(String bacResult) {
		this.bacResult = bacResult;
	}

	public String getIncidentTime() {
		return incidentTime;
	}

	public void setIncidentTime(String incidentTime) {
		this.incidentTime = incidentTime;
	}

	public String getTestTime() {
		return testTime;
	}

	public void setTestTime(String testTime) {
		this.testTime = testTime;
	}

	public int getMindiff() {
		return mindiff;
	}

	public void setMindiff(int mindiff) {
		this.mindiff = mindiff;
	}

}
