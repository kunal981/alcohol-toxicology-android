package com.brst.alcohol.toxicology.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.app.CaseDataAdapter;
import com.brst.alcohol.toxicology.base.BaseContainerFragment;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;

public class CaseProfile extends Fragment implements OnClickListener,
		OnItemSelectedListener, OnKeyListener {

	EditText drinkerName, drinkerWeight;
	Button btnNext;
	Spinner spinnerGender;

	boolean isGenderInit;

	String gender;

	CaseDataAdapter cData;
	CaseProfileModal cCaseData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cCaseData = CaseDataAdapter.getInstance().getCaseProfile();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_case_profile,
				container, false);
		drinkerName = (EditText) rootView
				.findViewById(R.id.id_text_case_name_value);
		drinkerWeight = (EditText) rootView
				.findViewById(R.id.text_weight_value);
		btnNext = (Button) rootView.findViewById(R.id.btn_next);
		spinnerGender = (Spinner) rootView.findViewById(R.id.spinner_gender);

		addTextChangeLisner();

		spinnerGender.setOnItemSelectedListener(this);

		drinkerName.setOnKeyListener(this);

		btnNext.setOnClickListener(this);
		return rootView;
	}

	// actionId == EditorInfo.IME_ACTION_DONE||

	private void addTextChangeLisner() {

		drinkerWeight
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if ((actionId == EditorInfo.IME_ACTION_DONE
								|| actionId == KeyEvent.ACTION_DOWN || actionId == KeyEvent.KEYCODE_BACK)) {
							if (drinkerWeight.getText().toString() != null
									&& !drinkerWeight.getText().toString()
											.trim().equals("")) {
								String weightString = drinkerWeight.getText()
										.toString();
								weightString = weightString + " lbs.";
								drinkerWeight.setText(weightString);
								drinkerWeight.setCursorVisible(false);
								hideKeyboard(getActivity(), drinkerWeight);

							}
							return true;
						}
						return false;
					}
				});

	}

	// Class util funcitons
	public static boolean hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		return imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.btn_next:

			String name = drinkerName.getText().toString();
			String weight = drinkerWeight.getText().toString();
			if (gender != null && name != null && !(name.equals(""))
					&& weight != null && !(weight.equals(""))) {
				cData = CaseDataAdapter.getInstance();
				cCaseData.setDrinkerName(name);
				cCaseData.setDrinkderGender(gender);

				cCaseData.setDrinkderWeight(splitWeigh(weight));
				DrinkEntry fragment = new DrinkEntry();
				cData.setCaseProfile(cCaseData);
				((BaseContainerFragment) getParentFragment()).replaceFragment(
						fragment, true);
			} else {

				Toast.makeText(getActivity(), "Required all field to enter",
						Toast.LENGTH_SHORT).show();
			}
			break;

		default:
			break;
		}

	}

	private String splitWeigh(String weight) {
		String[] str = weight.split(" ");
		return str[0];
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long arg3) {

		gender = parent.getItemAtPosition(position).toString();
		// Toast.makeText(getActivity(), "Gender: " + gender,
		// Toast.LENGTH_SHORT)
		// .show();

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		return false;
	}

}
