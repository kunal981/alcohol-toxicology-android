package com.brst.alcohol.toxicology.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.app.CaseDataAdapter;
import com.brst.alcohol.toxicology.base.BaseContainerFragment;
import com.brst.alcohol.toxicology.base.PlaceholderFragment;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;

public class Home extends Fragment implements OnClickListener {

	private LinearLayout btnMethodOne, btnMethodTwo, btnMethodThree,
			btnMethodFour;

	Button btnBack;

	CaseProfileModal cCaseData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cCaseData = CaseDataAdapter.getInstance().getCaseProfile();
		if (cCaseData == null) {
			cCaseData = new CaseProfileModal();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_main,
				container, false);
		btnMethodOne = (LinearLayout) rootView.findViewById(R.id.layout_bac);
		btnMethodTwo = (LinearLayout) rootView.findViewById(R.id.layout_re);
		btnMethodThree = (LinearLayout) rootView.findViewById(R.id.layout_im);
		btnMethodFour = (LinearLayout) rootView
				.findViewById(R.id.layout_drink_estimator);
		btnBack = (Button) rootView.findViewById(R.id.btn_header_back);
		// btnBack.setOnClickListener(this);
		btnMethodOne.setOnClickListener(this);
		btnMethodTwo.setOnClickListener(this);
		btnMethodThree.setOnClickListener(this);
		btnMethodFour.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.layout_bac:
			cCaseData.setCaseMethod(getString(R.string.text_method_1));
			CaseDataAdapter.getInstance().setCaseProfile(cCaseData);
			CaseProfile fragment = new CaseProfile();
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					fragment, true);

			break;
		case R.id.layout_re:
			cCaseData.setCaseMethod(getString(R.string.text_method_2));
			CaseDataAdapter.getInstance().setCaseProfile(cCaseData);
			RetroGradeEntryFragment fragment2 = new RetroGradeEntryFragment();
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					fragment2, true);
			break;
		case R.id.layout_im:
			ImpairmentFragment imFragment = new ImpairmentFragment();
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					imFragment, true);
			break;
		case R.id.layout_drink_estimator:
			DrinkEstimatorFragment drinkEstimatorFragment = new DrinkEstimatorFragment();
			((BaseContainerFragment) getParentFragment()).replaceFragment(
					drinkEstimatorFragment, true);
			break;

		// case R.id.btn_header_back:
		// getActivity().onBackPressed();
		// break;
		default:
			break;
		}

	}
}