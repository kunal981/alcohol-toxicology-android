package com.brst.alcohol.toxicology.home;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.alcohol.toxicology.R;

public class ImpairmentFragment extends Fragment {

	private static final String TAG = ImpairmentFragment.class.getName();

	private EditText textBacValue;
	private EditText textCaseName;

	LinearLayout listViewIm;
	LinearLayout listViewBeh;

	List<String> listBehaviorData;
	List<String> listImpairData;

	CustomListDataAdapter dataImp;
	CustomListDataAdapter dataBeh;

	// HashMap<String, List<String>> mapImpairment;
	// HashMap<String, List<String>> mapBehaviors;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_home_impairment,
				null);
		textBacValue = (EditText) rootView
				.findViewById(R.id.id_text_enter_bac_val);
		textCaseName = (EditText) rootView
				.findViewById(R.id.id_text_case_name_value);
		listViewIm = (LinearLayout) rootView
				.findViewById(R.id.list_impairments);
		listViewBeh = (LinearLayout) rootView.findViewById(R.id.list_behavior);
//		initListView();
		addtextSearchListner();
		return rootView;
	}

	private void addtextSearchListner() {

		textBacValue
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							if (!textBacValue.getText().toString().trim()
									.equals("")
									&& !textCaseName.getText().toString()
											.trim().equals("")) {
								Double bac = Double.valueOf(textBacValue
										.getText().toString());
								setupLists(bac);

								if (hideKeyboard(getActivity(), textBacValue)) {
									textBacValue.setText("");
								}
							} else {
								Toast.makeText(getActivity(), "Empty Text",
										Toast.LENGTH_SHORT).show();
							}
							return true;
						}
						return false;
					}
				});

	}

	// Class util funcitons
	public boolean hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		return imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

	}

	private void initListView() {

		// listImpairData = new ArrayList<String>();
		// listBehaviorData = new ArrayList<String>();
		// dataImp = new CustomListDataAdapter(getActivity(), listImpairData);
		// dataBeh = new CustomListDataAdapter(getActivity(), listBehaviorData);
		// listViewIm.setAdapter(dataImp);
		// listViewBeh.setAdapter(dataBeh);

		for (int i = 0; i < listImpairData.size(); i++) {
			View convertView = getActivity().getLayoutInflater().inflate(
					R.layout.layout_item_row_impariment, null);

			TextView item = (TextView) convertView
					.findViewById(R.id.text_concentration);

			item.setText(listImpairData.get(i));
			listViewIm.addView(convertView);
		}
		for (int i = 0; i < listBehaviorData.size(); i++) {
			View convertView = getActivity().getLayoutInflater().inflate(
					R.layout.layout_item_row_impariment, null);

			TextView item = (TextView) convertView
					.findViewById(R.id.text_concentration);

			item.setText(listBehaviorData.get(i));
			listViewBeh.addView(convertView);
		}

	}

	private void setupLists(double bacResult) {
		String impairmentId, behaviorId;
		String[] arrayImpariment, arrayBehavior;
		if (bacResult <= 0.060 && bacResult >= .010) {
			impairmentId = (getResources().getStringArray(R.array.impariment))[0];
			behaviorId = (getResources().getStringArray(R.array.behavior))[0];
			arrayImpariment = (getResources()
					.getStringArray(getArrayId(impairmentId)));
			arrayBehavior = (getResources()
					.getStringArray(getArrayId(behaviorId)));

			listImpairData = Arrays.asList(arrayImpariment);
			listBehaviorData = Arrays.asList(arrayBehavior);
			// listImpairData = mapImpairment.get(IConstant.KEY_ONE);
			// listBehaviorData = mapBehaviors.get(IConstant.KEY_ONE);
		} else if (bacResult <= 0.10 && bacResult > .060) {
			impairmentId = (getResources().getStringArray(R.array.impariment))[1];
			behaviorId = (getResources().getStringArray(R.array.behavior))[1];
			arrayImpariment = (getResources()
					.getStringArray(getArrayId(impairmentId)));
			arrayBehavior = (getResources()
					.getStringArray(getArrayId(behaviorId)));

			listImpairData = Arrays.asList(arrayImpariment);
			listBehaviorData = Arrays.asList(arrayBehavior);
			// listImpairData = mapImpairment.get(IConstant.KEY_TWO);
			// listBehaviorData = mapBehaviors.get(IConstant.KEY_TWO);

		} else if (bacResult <= 0.20 && bacResult > 0.10) {
			impairmentId = (getResources().getStringArray(R.array.impariment))[2];
			behaviorId = (getResources().getStringArray(R.array.behavior))[2];
			arrayImpariment = (getResources()
					.getStringArray(getArrayId(impairmentId)));
			arrayBehavior = (getResources()
					.getStringArray(getArrayId(behaviorId)));

			listImpairData = Arrays.asList(arrayImpariment);
			listBehaviorData = Arrays.asList(arrayBehavior);

			// listImpairData = mapImpairment.get(IConstant.KEY_THREE);
			// listBehaviorData = mapBehaviors.get(IConstant.KEY_THREE);

		} else if (bacResult <= 0.29 && bacResult > .20) {

			impairmentId = (getResources().getStringArray(R.array.impariment))[3];
			behaviorId = (getResources().getStringArray(R.array.behavior))[3];
			arrayImpariment = (getResources()
					.getStringArray(getArrayId(impairmentId)));
			arrayBehavior = (getResources()
					.getStringArray(getArrayId(behaviorId)));

			listImpairData = Arrays.asList(arrayImpariment);
			listBehaviorData = Arrays.asList(arrayBehavior);

			// listImpairData = mapImpairment.get(IConstant.KEY_FOUR);
			// listBehaviorData = mapBehaviors.get(IConstant.KEY_FOUR);

		} else if (bacResult <= 0.39 && bacResult > .29) {
			impairmentId = (getResources().getStringArray(R.array.impariment))[4];
			behaviorId = (getResources().getStringArray(R.array.behavior))[4];
			arrayImpariment = (getResources()
					.getStringArray(getArrayId(impairmentId)));
			arrayBehavior = (getResources()
					.getStringArray(getArrayId(behaviorId)));

			listImpairData = Arrays.asList(arrayImpariment);
			listBehaviorData = Arrays.asList(arrayBehavior);

			// listImpairData = mapImpairment.get(IConstant.KEY_FIVE);
			// listBehaviorData = mapBehaviors.get(IConstant.KEY_FIVE);

		} else if (bacResult > .40) {
			impairmentId = (getResources().getStringArray(R.array.impariment))[5];
			behaviorId = (getResources().getStringArray(R.array.behavior))[5];
			arrayImpariment = (getResources()
					.getStringArray(getArrayId(impairmentId)));
			arrayBehavior = (getResources()
					.getStringArray(getArrayId(behaviorId)));

			listImpairData = Arrays.asList(arrayImpariment);
			listBehaviorData = Arrays.asList(arrayBehavior);
			// listImpairData = mapImpairment.get(IConstant.KEY_SIX);
			// listBehaviorData = mapBehaviors.get(IConstant.KEY_SIX);

		} else {
			listImpairData = Arrays.asList("No Information");
			listBehaviorData = Arrays.asList("No Information");

		}
		// dataImp.addItem(listImpairData);
		// dataBeh.addItem(listBehaviorData);
		initListView();

	}

	private int getArrayId(String arranameString) {

		int id = getResources().getIdentifier(arranameString, "array",
				getActivity().getPackageName());

		return id;

	}

	class CustomListDataAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		private List<String> data;

		public CustomListDataAdapter(Activity activity, List<String> data) {
			this.activity = activity;
			this.data = data;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int location) {
			return data.get(location);
		}

		public void addItem(List<String> data) {
			this.data = data;
			notifyDataSetChanged();
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
				convertView = inflater.inflate(
						R.layout.layout_item_row_impariment, null);

			TextView item = (TextView) convertView
					.findViewById(R.id.text_concentration);

			item.setText(data.get(position));

			return convertView;
		}
	}

	// private void initImparimentAndBehaviorData() {
	//
	// String[] str_i_1 = { "Concentration", "Thought", "Judgment",
	// "Coordination" };
	//
	// String[] str_i_2 = { "Impaired Reflexes", "Reasoning",
	// "Depth Perception", "Distance Acuity", "Peripheral Vision",
	// "Glare Recover" };
	//
	// String[] str_i_3 = { "Reaction Time", "Gross Motor Control",
	// "Staggering", "Slurred Speech" };
	//
	// String[] str_i_4 = { "Severe Motor", "Impairment",
	// "Loss of Consciousness", "Memory Blackout" };
	//
	// String[] str_i_5 = { "Bladder Function", "Breathing", "Heart Rate" };
	//
	// String[] str_i_6 = { "Breathing", "Heart Rate" };
	//
	// String[] str_b_1 = { "Lowered Alertness", "Disinhibition",
	// "Sense of Well-Being", "Relaxation" };
	//
	// String[] str_b_2 = { "Blunted Feeling", "Disinhibition",
	// "Extroversion", "Impaired Sexual Pleasure" };
	//
	// String[] str_b_3 = { "Over-Expression", "Emotional Swings",
	// "Angry or Sad", "Boisterous" };
	//
	// String[] str_b_4 = { "Stupor", "Loss of Understanding",
	// "Impaired Sensations" };
	// String[] str_b_5 = { "Severe Depression", "Unconsciousness",
	// "Death Possible" };
	// String[] str_b_6 = { "Unconsciousness", "Death" };
	//
	// mapImpairment = new HashMap<String, List<String>>();
	// mapImpairment.put(IConstant.KEY_ONE, Arrays.asList(str_i_1));
	// mapImpairment.put(IConstant.KEY_TWO, Arrays.asList(str_i_2));
	// mapImpairment.put(IConstant.KEY_THREE, Arrays.asList(str_i_3));
	// mapImpairment.put(IConstant.KEY_FOUR, Arrays.asList(str_i_4));
	// mapImpairment.put(IConstant.KEY_FIVE, Arrays.asList(str_i_5));
	// mapImpairment.put(IConstant.KEY_SIX, Arrays.asList(str_i_6));
	//
	// mapBehaviors = new HashMap<String, List<String>>();
	// mapBehaviors.put(IConstant.KEY_ONE, Arrays.asList(str_b_1));
	// mapBehaviors.put(IConstant.KEY_TWO, Arrays.asList(str_b_2));
	// mapBehaviors.put(IConstant.KEY_THREE, Arrays.asList(str_b_3));
	// mapBehaviors.put(IConstant.KEY_FOUR, Arrays.asList(str_b_4));
	// mapBehaviors.put(IConstant.KEY_FIVE, Arrays.asList(str_b_5));
	// mapBehaviors.put(IConstant.KEY_SIX, Arrays.asList(str_b_6));
	//
	// }

}
