package com.brst.alcohol.toxicology.home;

import java.util.Calendar;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.app.CaseDataAdapter;
import com.brst.alcohol.toxicology.base.BaseContainerFragment;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;
import com.brst.alcohol.toxicology.modal.CaseRetrogradeProfileModel;
import com.brst.alcohol.toxicology.util.LogMsg;
import com.brst.alcohol.toxicology.util.TimeUtil;

public class RetroGradeEntryFragment extends Fragment implements
		OnClickListener {

	String time;

	EditText caseName;
	EditText bacTestValue;
	EditText testTime;
	EditText incidentTime;
	Button calaculate;

	CaseDataAdapter cData;
	CaseRetrogradeProfileModel caseRetrogradeProfileModel;
	CaseProfileModal cCaseData;

	private static String TAG = "RetroGradeEntryFragment";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			Log.e(TAG, savedInstanceState.getString("message"));
		}
		Log.e(TAG, "onCreate");
		cData = CaseDataAdapter.getInstance();
		cCaseData = cData.getCaseProfile();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");

		View rootView = inflater.inflate(
				R.layout.fragemet_layout_retrograde_profile, null);
		caseName = (EditText) rootView
				.findViewById(R.id.id_text_case_name_value);
		bacTestValue = (EditText) rootView
				.findViewById(R.id.id_bac_test_result_value);
		testTime = (EditText) rootView.findViewById(R.id.id_time_test_value);
		calaculate = (Button) rootView.findViewById(R.id.btn_calulate);
		calaculate.setOnClickListener(this);
		incidentTime = (EditText) rootView
				.findViewById(R.id.id_incident_time_value);

		incidentTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);
				TimePickerDialog mTimePicker;
				mTimePicker = new TimePickerDialog(getActivity(),
						new TimePickerDialog.OnTimeSetListener() {
							@Override
							public void onTimeSet(TimePicker timePicker,
									int hourOfDay, int minute) {
								TimeUtil.hourUnitIncident = hourOfDay;
								TimeUtil.minUnitIncident = minute;
								TimeUtil.isTestTimeSet = true;

								StringBuilder sb = new StringBuilder();

								String timeFormat = hourOfDay > 12 ? "PM"
										: "AM";

								String hr = hourOfDay > 12 ? (hourOfDay - 12)
										+ "" : hourOfDay + "";

								sb.append(hr).append(":")
										.append(String.format("%02d", minute))
										.append(timeFormat);
								time = sb.toString();

								incidentTime.setText(time);
							}
						}, hour, minute, true);// Yes 24 hour time
				mTimePicker.setTitle("Incident Time");
				mTimePicker.show();

			}
		});

		testTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);
				TimePickerDialog mTimePicker;
				mTimePicker = new TimePickerDialog(getActivity(),
						new TimePickerDialog.OnTimeSetListener() {
							@Override
							public void onTimeSet(TimePicker timePicker,
									int hourOfDay, int minute) {
								if (TimeUtil.isTestTimeSet) {
									TimeUtil.hourUnitTestTime = hourOfDay;
									TimeUtil.minUnitTestTime = minute;

									if (TimeUtil.isTestTimeValid()) {
										String time;

										StringBuilder sb = new StringBuilder();

										String timeFormat = hourOfDay > 12 ? "PM"
												: "AM";

										String hr = hourOfDay > 12 ? (hourOfDay - 12)
												+ ""
												: hourOfDay + "";

										sb.append(hr)
												.append(":")
												.append(String.format("%02d",
														minute))
												.append(timeFormat);
										time = sb.toString();

										testTime.setText(time);
									} else {
										LogMsg.LOG(getActivity(),
												"Test time should be greater then incident time");
									}

								} else {
									LogMsg.LOG(getActivity(),
											"Set incident time first");
								}

							}
						}, hour, minute, true);// Yes 24 hour time
				mTimePicker.setTitle("Test time");
				mTimePicker.show();

			}
		});

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		TimeUtil.initTimeUnit();
		Log.e(TAG, "onActivityCreated");

	}

	@Override
	public void onResume() {
		super.onResume();
		Log.e(TAG, "onResume");
		testTime.setText("00:00");
		incidentTime.setText("00:00");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.e(TAG, "onPause");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.e(TAG, "onSaveInstanceState");
		outState.putString("message", "This is my message to be reloaded");
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.btn_calulate:
			String strcaseName = caseName.getText().toString().trim();
			String strbacTestValue = bacTestValue.getText().toString().trim();
			String strIncidentTime = incidentTime.getText().toString().trim();
			String strTestTime = testTime.getText().toString().trim();

			if (strcaseName != null && !strcaseName.equals("")
					&& strbacTestValue != null && !strbacTestValue.equals("")
					&& TimeUtil.getMinuteDifferene() > 0) {
				cData = CaseDataAdapter.getInstance();
				caseRetrogradeProfileModel = new CaseRetrogradeProfileModel(
						strcaseName, strbacTestValue, strIncidentTime,
						strTestTime, TimeUtil.getMinuteDifferene());
				cCaseData.setDrinkerName(strcaseName);
				cCaseData.setCreateOn(TimeUtil.getCurrentDate());

				cData.setCaseRetroGradeModel(caseRetrogradeProfileModel);
				cData.setCaseProfile(cCaseData);

				if (TimeUtil.getMinuteDifferene() > 45) {
					RetroGradeCaseTwoResultFrgment fragment = new RetroGradeCaseTwoResultFrgment();
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(fragment, true);
				} else {
					RetroGradeCaseOneResultFragment fragment = new RetroGradeCaseOneResultFragment();
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(fragment, true);
				}
			}
			break;
		default:
			break;

		}

	}

}
