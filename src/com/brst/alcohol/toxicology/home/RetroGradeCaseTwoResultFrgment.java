package com.brst.alcohol.toxicology.home;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.app.CaseDataAdapter;
import com.brst.alcohol.toxicology.db.DbOperation;
import com.brst.alcohol.toxicology.home.Result.InsertDrinkAsync;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;
import com.brst.alcohol.toxicology.modal.CaseRetrogradeProfileModel;
import com.brst.alcohol.toxicology.util.IConstant;

public class RetroGradeCaseTwoResultFrgment extends Fragment implements
		OnClickListener {

	CaseDataAdapter cAdapter;

	EditText bacValue;
	TextView casename;
	TextView title;

	LinearLayout listViewIm;
	LinearLayout listViewBeh;

	HashMap<String, List<String>> mapImpairment;
	HashMap<String, List<String>> mapBehaviors;

	List<String> listBehaviorData;
	List<String> listImpairData;

	CustomListDataAdapter dataImp;
	CustomListDataAdapter dataBeh;

	CaseRetrogradeProfileModel caseRetrogradeProfileModel;

	double bacResult = 0.0;
	String bacValueStr;

	private static String TAG = "RetroGradeCaseTwoResultFrgment";

	CaseProfileModal cCaseData;
	ProgressDialog progressDailog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			Log.e(TAG, savedInstanceState.getString("message"));
		}
		progressDailog = new ProgressDialog(getActivity());
		Log.e(TAG, "onCreate");
		cAdapter = CaseDataAdapter.getInstance();
		caseRetrogradeProfileModel = cAdapter.getCaseRetroGradeModel();
		cCaseData = cAdapter.getCaseProfile();
		bacResult = calCalculateBac();
		bacValueStr = new DecimalFormat("#0.000").format(bacResult) + " g/dL";
		cCaseData.setBacValue(bacValueStr);
		cAdapter.setCaseProfile(cCaseData);
		new InsertDrinkAsync(progressDailog).execute(cCaseData);
		Log.e(TAG, "onCreate");

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		View rootView = inflater.inflate(R.layout.fragment_home_bac_result,
				null);

		casename = (TextView) rootView.findViewById(R.id.text_case_name);
		casename.setText(caseRetrogradeProfileModel.getCaseName());

		bacValue = (EditText) rootView.findViewById(R.id.id_text_bac_value);
		listViewIm = (LinearLayout) rootView
				.findViewById(R.id.list_impairments);
		listViewBeh = (LinearLayout) rootView.findViewById(R.id.list_behavior);
		title = (TextView) rootView.findViewById(R.id.header_title);
		title.setText(getString(R.string.header_title_alcohol_toxicology_result));

		initImparimentAndBehaviorData();

		setupLists(bacResult);

		bacValue.setText(bacValueStr);
		initListView();

		return rootView;
	}

	private double calCalculateBac() {
		double bac = Double.parseDouble(caseRetrogradeProfileModel
				.getBacResult())
				+ ((caseRetrogradeProfileModel.getMindiff() / 60) * .015);

		return bac;
	}

	private void initListView() {

		// dataImp = new CustomListDataAdapter(getActivity(), listImpairData);
		// dataBeh = new CustomListDataAdapter(getActivity(), listBehaviorData);
		// listViewIm.setAdapter(dataImp);
		// listViewBeh.setAdapter(dataBeh);

		for (int i = 0; i < listImpairData.size(); i++) {
			View convertView = getActivity().getLayoutInflater().inflate(
					R.layout.layout_item_row_impariment, null);

			TextView item = (TextView) convertView
					.findViewById(R.id.text_concentration);

			item.setText(listImpairData.get(i));
			listViewIm.addView(convertView);
		}
		for (int i = 0; i < listBehaviorData.size(); i++) {
			View convertView = getActivity().getLayoutInflater().inflate(
					R.layout.layout_item_row_impariment, null);

			TextView item = (TextView) convertView
					.findViewById(R.id.text_concentration);

			item.setText(listBehaviorData.get(i));
			listViewBeh.addView(convertView);
		}

	}

	private void setupLists(double bacResult) {

		if (bacResult <= 0.060 && bacResult >= .010) {
			listImpairData = mapImpairment.get(IConstant.KEY_ONE);
			listBehaviorData = mapBehaviors.get(IConstant.KEY_ONE);
		} else if (bacResult <= 0.10 && bacResult > .060) {
			listImpairData = mapImpairment.get(IConstant.KEY_TWO);
			listBehaviorData = mapBehaviors.get(IConstant.KEY_TWO);

		} else if (bacResult <= 0.20 && bacResult > 0.10) {
			listImpairData = mapImpairment.get(IConstant.KEY_THREE);
			listBehaviorData = mapBehaviors.get(IConstant.KEY_THREE);

		} else if (bacResult <= 0.29 && bacResult > .20) {
			listImpairData = mapImpairment.get(IConstant.KEY_FOUR);
			listBehaviorData = mapBehaviors.get(IConstant.KEY_FOUR);

		} else if (bacResult <= 0.39 && bacResult > .29) {
			listImpairData = mapImpairment.get(IConstant.KEY_FIVE);
			listBehaviorData = mapBehaviors.get(IConstant.KEY_FIVE);

		} else if (bacResult > .40) {
			listImpairData = mapImpairment.get(IConstant.KEY_SIX);
			listBehaviorData = mapBehaviors.get(IConstant.KEY_SIX);

		} else {
			listImpairData = Arrays.asList("No Information");
			listBehaviorData = Arrays.asList("No Information");

		}

	}

	private void initImparimentAndBehaviorData() {

		String[] str_i_1 = { "Concentration", "Thought", "Judgment",
				"Coordination" };

		String[] str_i_2 = { "Impaired Reflexes", "Reasoning",
				"Depth Perception", "Distance Acuity", "Peripheral Vision",
				"Glare Recover" };

		String[] str_i_3 = { "Reaction Time", "Gross Motor Control",
				"Staggering", "Slurred Speech" };

		String[] str_i_4 = { "Severe Motor Impairment",
				"Loss of Consciousness", "Memory Blackout" };

		String[] str_i_5 = { "Bladder Function", "Breathing", "Heart Rate" };

		String[] str_i_6 = { "Breathing", "Heart Rate" };

		String[] str_b_1 = { "Lowered Alertness", "Disinhibition",
				"Sense of Well-Being", "Relaxation" };

		String[] str_b_2 = { "Blunted Feeling", "Disinhibition",
				"Extroversion", "Impaired Sexual Pleasure" };

		String[] str_b_3 = { "Over-Expression", "Emotional Swings",
				"Angry or Sad", "Boisterous" };

		String[] str_b_4 = { "Stupor", "Loss of Understanding",
				"Impaired Sensations" };
		String[] str_b_5 = { "Severe Depression", "Unconsciousness",
				"Death Possible" };
		String[] str_b_6 = { "Unconsciousness", "Death" };

		mapImpairment = new HashMap<String, List<String>>();
		mapImpairment.put(IConstant.KEY_ONE, Arrays.asList(str_i_1));
		mapImpairment.put(IConstant.KEY_TWO, Arrays.asList(str_i_2));
		mapImpairment.put(IConstant.KEY_THREE, Arrays.asList(str_i_3));
		mapImpairment.put(IConstant.KEY_FOUR, Arrays.asList(str_i_4));
		mapImpairment.put(IConstant.KEY_FIVE, Arrays.asList(str_i_5));
		mapImpairment.put(IConstant.KEY_SIX, Arrays.asList(str_i_6));

		mapBehaviors = new HashMap<String, List<String>>();
		mapBehaviors.put(IConstant.KEY_ONE, Arrays.asList(str_b_1));
		mapBehaviors.put(IConstant.KEY_TWO, Arrays.asList(str_b_2));
		mapBehaviors.put(IConstant.KEY_THREE, Arrays.asList(str_b_3));
		mapBehaviors.put(IConstant.KEY_FOUR, Arrays.asList(str_b_4));
		mapBehaviors.put(IConstant.KEY_FIVE, Arrays.asList(str_b_5));
		mapBehaviors.put(IConstant.KEY_SIX, Arrays.asList(str_b_6));

	}

	class CustomListDataAdapter extends BaseAdapter {
		private Activity activity;
		private LayoutInflater inflater;
		private List<String> data;

		public CustomListDataAdapter(Activity activity, List<String> data) {
			this.activity = activity;
			this.data = data;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int location) {
			return data.get(location);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null)
				convertView = inflater.inflate(
						R.layout.layout_item_row_impariment, null);

			TextView item = (TextView) convertView
					.findViewById(R.id.text_concentration);

			item.setText(data.get(position));

			return convertView;
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			Log.e(TAG, savedInstanceState.getString("message"));
		}
		Log.e(TAG, "onActivityCreated");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.e(TAG, "onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.e(TAG, "onPause");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.e(TAG, "onSaveInstanceState");
		outState.putString("message", "This is my message to be reloaded");
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.btn_header_back:
			getActivity().onBackPressed();
			break;

		default:
			break;
		}
	}

	class InsertDrinkAsync extends AsyncTask<CaseProfileModal, Void, Long> {
		String TAG = "InsertCaseAsync";
		ProgressDialog progress;
		DbOperation db;

		public InsertDrinkAsync(ProgressDialog progress) {
			this.progress = progress;
			this.db = new DbOperation(getActivity());
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.show();
		}

		@Override
		protected Long doInBackground(CaseProfileModal... params) {
			long status = -1;
			status = db.insertInsertCaseData(params[0]);
			return status;
		}

		@Override
		protected void onPostExecute(Long result) {

			if (progress.isShowing()) {
				progress.dismiss();
			}
			if (result != -1) {
				cCaseData.setCaseId(result);
				cAdapter.setCaseProfile(cCaseData);

			} else {
				Toast.makeText(getActivity(), "Error on Case Updation",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

}
