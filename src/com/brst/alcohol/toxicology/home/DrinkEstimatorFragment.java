package com.brst.alcohol.toxicology.home;

import java.text.DecimalFormat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.util.LogMsg;

public class DrinkEstimatorFragment extends Fragment implements
		OnClickListener, OnItemSelectedListener {

	EditText drinkerName, drinkerWeight, drinkerBacCount;
	Spinner spinnerGender;

	TextView result;

	boolean isGenderInit;

	String gender;
	double bacValue, weight;

	Double alcoholAmount;
	Double alcoholAmtInFlz;
	double totalDrink;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_home_drink_estimation, null);
		result = (TextView) rootView.findViewById(R.id.text_result);
		drinkerName = (EditText) rootView
				.findViewById(R.id.id_text_case_name_value);
		drinkerWeight = (EditText) rootView
				.findViewById(R.id.text_weight_value);
		drinkerBacCount = (EditText) rootView.findViewById(R.id.text_bac_value);
		spinnerGender = (Spinner) rootView.findViewById(R.id.spinner_gender);

		addTextChangeLisner();

		spinnerGender.setOnItemSelectedListener(this);

		return rootView;
	}

	// actionId == EditorInfo.IME_ACTION_DONE||

	private void addTextChangeLisner() {

		drinkerName
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if ((actionId == EditorInfo.IME_ACTION_NEXT)) {

							drinkerWeight.requestFocus();
							drinkerWeight.setText("");
							hideKeyboard(getActivity(), drinkerWeight);

							return true;
						}
						return false;
					}
				});

		drinkerWeight
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if ((actionId == EditorInfo.IME_ACTION_NEXT)) {
							if (drinkerWeight.getText().toString() != null
									&& !drinkerWeight.getText().toString()
											.trim().equals("")) {
								String weightString = drinkerWeight.getText()
										.toString();
								weight = Double.parseDouble(weightString);
								weightString = weightString + " lbs.";
								drinkerWeight.setText(weightString);
								drinkerBacCount.requestFocus();
								hideKeyboard(getActivity(), drinkerWeight);

							}
							return true;
						}
						return false;
					}
				});
		drinkerBacCount
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if ((actionId == EditorInfo.IME_ACTION_DONE)) {
							if (checkField()) {
								String weightString = drinkerBacCount.getText()
										.toString();
								bacValue = Double.parseDouble(weightString);
								calculationDrinks();
								hideKeyboard(getActivity(), drinkerBacCount);

							} else {
								hideKeyboard(getActivity(), drinkerBacCount);
							}
							return true;
						}
						return false;
					}
				});

	}

	protected int calculationDrinks() {
		String resultValue = null;
		alcoholAmount = calculateAlcoholAmount(bacValue, weight,
				getSexRatio(gender));
		alcoholAmtInFlz = calculateAlcoholAmountInFloz(alcoholAmount);
		totalDrink = totalAmountOfDrink(alcoholAmtInFlz);

		if (totalDrink > 0.25) {
			if (totalDrink < 1) {
				resultValue = new DecimalFormat("#0.000").format(totalDrink)
						+ " Total Number of Drinks";
			} else {
				int drinks = (int) totalDrink;
				resultValue = String.valueOf(drinks)
						+ " Total Number of Drinks";
			}

		} else {
			resultValue = String.valueOf(0.25) + " Total Number of Drinks";
		}
		result.setVisibility(View.VISIBLE);
		result.setText(resultValue);
		drinkerWeight.setText("");
		drinkerBacCount.setText("");
		return 0;

	}

	private Double totalAmountOfDrink(Double alcohol) {
		return (alcohol / 0.6);
	}

	private Double calculateAlcoholAmountInFloz(Double alcoholAmount) {
		return alcoholAmount / 29.6;
	}

	private Double calculateAlcoholAmount(double bac, double weight,
			double sexRatio) {

		double amount = (bac * (weight * 454) * sexRatio) / 78.9;
		return amount;
	}

	private Double getSexRatio(String gender) {
		if (gender.toUpperCase().equals("MALE")) {
			return .68;
		} else {
			return .55;
		}
	}

	protected boolean checkField() {
		if (drinkerName.getText().toString().trim().equals("")) {
			LogMsg.LOG(getActivity(), "Please enter case name");
			drinkerName.requestFocus();
			return false;
		}
		if (drinkerWeight.getText().toString().trim().equals("")) {
			LogMsg.LOG(getActivity(), "Please enter Weight");
			drinkerWeight.requestFocus();
			return false;
		}
		if (drinkerBacCount.getText().toString().trim().equals("")) {
			LogMsg.LOG(getActivity(), "Please enter BAC value");
			drinkerBacCount.requestFocus();
			return false;
		}
		if (gender.equals("")) {
			LogMsg.LOG(getActivity(), "Select gender from list");
			return false;
		}

		return true;
	}

	// Class util funcitons
	public static boolean hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		return imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

	}

	public static boolean showKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		return imm.showSoftInput(view, 0);

	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

	}

	private String splitWeigh(String weight) {
		String[] str = weight.split(" ");
		return str[0];
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long arg3) {

		gender = parent.getItemAtPosition(position).toString();
		// Toast.makeText(getActivity(), "Gender: " + gender,
		// Toast.LENGTH_SHORT)
		// .show();

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

}
