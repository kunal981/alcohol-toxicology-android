package com.brst.alcohol.toxicology.home;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.alcohol.toxicology.MainActivity;
import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.db.DbOperation;
import com.brst.alcohol.toxicology.modal.Drink;
import com.brst.alcohol.toxicology.util.IConstant;

public class CustomDrinkDailogActivity extends Activity implements
		OnClickListener {

	ListView listviewDrink;
	Button btnCreateDrink;
	Button btnCancelDrink;
	Button btnCreate;

	EditText drinkNameValue;
	EditText drinkAlcoholValue;
	EditText drinkVolValue;

	TextView txtNoDrink;
	private PopupWindow popWindow;

	List<Drink> items;

	CustomDrinkAdapter adapter;

	ProgressDialog progressDailog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layout_dailog_custom_drink);

		progressDailog = new ProgressDialog(this);

		listviewDrink = (ListView) findViewById(R.id.list_add_drink);

		btnCreateDrink = (Button) findViewById(R.id.btn_create_drink);
		btnCancelDrink = (Button) findViewById(R.id.btn_cancel_drink);

		txtNoDrink = (TextView) findViewById(R.id.text_no_drink);

		btnCreateDrink.setOnClickListener(this);
		btnCancelDrink.setOnClickListener(this);

		new GetDrinkAsync(progressDailog, "create").execute();

	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();

		switch (id) {
		case R.id.btn_create_drink:
			initiatePopupWindow();
			break;
		case R.id.btn_cancel_drink:
			finish();
			break;
		case R.id.btn_create:
			// Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
			creteDrink();
			updateListUI();
			break;

		default:
			break;
		}

	}

	private void updateListUI() {

		if (adapter.getCount() > 0) {
			listviewDrink.setVisibility(View.VISIBLE);
			txtNoDrink.setVisibility(View.GONE);
		} else {
			listviewDrink.setVisibility(View.GONE);
			txtNoDrink.setVisibility(View.VISIBLE);
		}

	}

	private void creteDrink() {
		String drinkName = drinkNameValue.getText().toString();
		String drinkAlcohol = drinkAlcoholValue.getText().toString();
		String drinkVolume = drinkVolValue.getText().toString();

		if (drinkAlcohol != null && !drinkAlcohol.trim().equals("")
				&& drinkName != null && !drinkName.trim().equals("")
				&& drinkVolume != null && !drinkVolume.trim().equals("")) {

			double alcoholQ = Double.parseDouble(drinkAlcohol);
			double volumeQ = Double.parseDouble(drinkVolume);
			Drink drinkCreate = new Drink(IConstant.DRINK_5, drinkName,
					alcoholQ, volumeQ);

			new InsertDrinkAsync(progressDailog).execute(drinkCreate);

		} else {
			Toast.makeText(this, "All Fields value require", Toast.LENGTH_SHORT)
					.show();
		}
		/*
		 * double drinkAlcohol = Double.parseDouble(drinkAlcoholValue.getText()
		 * .toString());
		 */

	}

	// Background task to retrive custom Drink list from database
	class GetDrinkAsync extends AsyncTask<Object, Object, List<Drink>> {
		String TAG = "GetDrinkAsync Task";
		ProgressDialog progress;
		DbOperation db;
		String type;

		public GetDrinkAsync(ProgressDialog progress, String type) {
			this.type = type;
			this.progress = progress;
			this.db = new DbOperation(CustomDrinkDailogActivity.this);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.show();
		}

		@Override
		protected List<Drink> doInBackground(Object... params) {
			ArrayList<Drink> arrayList = null;

			try {
				arrayList = db.getAllCustomDrink();
			} catch (SQLException e) {
				arrayList = new ArrayList<>();
				Log.e(TAG, e.getStackTrace() + "");
			}

			return arrayList;
		}

		@Override
		protected void onPostExecute(List<Drink> result) {

			if (progress.isShowing()) {
				progress.dismiss();
			}

			if (type.equals("create")) {
				adapter = new CustomDrinkAdapter(
						CustomDrinkDailogActivity.this, result);
				listviewDrink.setAdapter(adapter);
			} else if (type.equals("update")) {
				adapter.updateItem(result);
			}
			updateListUI();

		}
	}

	// Background task to insert custom Drink list to database
	class InsertDrinkAsync extends AsyncTask<Drink, Void, Boolean> {
		String TAG = "InsertDrinkAsync";
		ProgressDialog progress;
		DbOperation db;

		public InsertDrinkAsync(ProgressDialog progress) {
			this.progress = progress;
			this.db = new DbOperation(CustomDrinkDailogActivity.this);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.show();
		}

		@Override
		protected Boolean doInBackground(Drink... params) {
			boolean status = false;
			try {
				status = db.insertCustomDrink(params[0]);
			} catch (SQLException e) {
				Log.e(TAG, e.getStackTrace() + "");
			}
			return status;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (progress.isShowing()) {
				progress.dismiss();
			}
			if (result) {
				new GetDrinkAsync(progressDailog, "update").execute();
				popWindow.dismiss();
			} else {
				popWindow.dismiss();
				Toast.makeText(getApplicationContext(),
						"Error while saving Drink", Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void initiatePopupWindow() {
		LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View popupView = layoutInflater.inflate(
				R.layout.layout_popup_window_custom_drink, null);
		popWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT, true);

		popWindow.showAtLocation(btnCreateDrink, Gravity.CENTER, 0, 0);

		drinkNameValue = (EditText) popupView	
				.findViewById(R.id.id_text_drink_name_val);
		drinkAlcoholValue = (EditText) popupView
				.findViewById(R.id.id_text_drink_alcohol_val);
		drinkVolValue = (EditText) popupView
				.findViewById(R.id.id_text_drink_vol_val);

		btnCreate = (Button) popupView.findViewById(R.id.btn_create);
		btnCreate.setOnClickListener(this);

	}

	class CustomDrinkAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		private List<Drink> listDrinks;

		public CustomDrinkAdapter(Activity activity, List<Drink> listDrinks) {
			super();
			this.activity = activity;
			this.listDrinks = listDrinks;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listDrinks.size();
		}

		@Override
		public Object getItem(int location) {
			// TODO Auto-generated method stub
			return listDrinks.get(location);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public void updateItem(List<Drink> listDrinks) {
			this.listDrinks = listDrinks;
			notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder;

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_list_row, null);

				holder = new ViewHolder();
				holder.drinkName = (EditText) convertView
						.findViewById(R.id.id_text_drink);
				holder.drinkDesc = (EditText) convertView
						.findViewById(R.id.id_text_drink_value);

				holder.iconAdd = (ImageView) convertView
						.findViewById(R.id.img_text_cancel);
				holder.iconAdd.setImageResource(R.drawable.btn_plus);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final Drink item = listDrinks.get(position);
			holder.drinkName.setText(item.getName().toString());
			String percent = item.getPercentAlcohol() + "";
			String volume = item.getVolumeAlcohol() + "";
			StringBuilder str = new StringBuilder();
			str.append(percent).append("% ").append(volume).append(" fl.oz");
			holder.drinkDesc.setText(str.toString());

			holder.iconAdd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					// Toast.makeText(CustomDrinkDailogActivity.this,
					// "Coming soon", Toast.LENGTH_SHORT).show();
					Intent i = getIntent();
					i.putExtra("item", item);
					setResult(RESULT_OK, i);
					finish();
				}

			});
			return convertView;
		}

	}

	static class ViewHolder {
		EditText drinkName;
		EditText drinkDesc;
		ImageView iconAdd;
	}
}
