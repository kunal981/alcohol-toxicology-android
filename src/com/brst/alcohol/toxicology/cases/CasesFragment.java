package com.brst.alcohol.toxicology.cases;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.base.BaseContainerFragment;
import com.brst.alcohol.toxicology.db.DbOperation;
import com.brst.alcohol.toxicology.home.CaseProfile;
import com.brst.alcohol.toxicology.home.Result;
import com.brst.alcohol.toxicology.modal.CaseProfileModal;
import com.brst.alcohol.toxicology.util.LogMsg;

public class CasesFragment extends Fragment {

	List<CaseProfileModal> listItems;
	ListView list;
	TextView textNoInfo;

	CustomCasesAdapter adapter;
	ProgressDialog progress;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listItems = new ArrayList<CaseProfileModal>();
		// progress = new ProgressDialog(getActivity());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_layout_cases, null);
		list = (ListView) rootView.findViewById(R.id.list_cases);
		textNoInfo = (TextView) rootView.findViewById(R.id.noData);
		adapter = new CustomCasesAdapter(getActivity(), listItems);
		list.setAdapter(adapter);

		new GetCasesAsync().execute();
		return rootView;

	}

	class GetCasesAsync extends
			AsyncTask<Object, Object, List<CaseProfileModal>> {
		String TAG = "GetDrinkAsync Task";
		ProgressDialog progress;
		DbOperation db;

		public GetCasesAsync() {
			this.db = new DbOperation(getActivity());
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected List<CaseProfileModal> doInBackground(Object... params) {
			ArrayList<CaseProfileModal> arrayList = null;

			try {

				arrayList = db.getAllCases();
			} catch (SQLException e) {
				arrayList = new ArrayList<>();
				Log.e(TAG, e.getStackTrace() + "");
			}

			return arrayList;
		}

		@Override
		protected void onPostExecute(List<CaseProfileModal> result) {

			if (result != null) {
				UpdateUi(result);
			}

		}
	}

	class CustomCasesAdapter extends BaseAdapter {

		private Activity activity;
		private LayoutInflater inflater;
		private List<CaseProfileModal> listItems;

		public CustomCasesAdapter(Activity activity,
				List<CaseProfileModal> listItems) {
			super();
			this.activity = activity;
			this.listItems = listItems;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return listItems.size();
		}

		@Override
		public Object getItem(int location) {
			// TODO Auto-generated method stub
			return listItems.get(location);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public void updateItem(List<CaseProfileModal> listItems) {
			this.listItems = listItems;
			notifyDataSetChanged();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewHolder holder;

			if (inflater == null)
				inflater = (LayoutInflater) activity
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.layout_list_row_cases,
						null);

				holder = new ViewHolder();
				holder.name = (TextView) convertView.findViewById(R.id.name);
				holder.date = (TextView) convertView.findViewById(R.id.date);

				holder.method = (TextView) convertView
						.findViewById(R.id.method);
				holder.val = (TextView) convertView.findViewById(R.id.bacValue);
				holder.delete = (ImageView) convertView
						.findViewById(R.id.deleteEntry);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final CaseProfileModal item = listItems.get(position);
			holder.name.setText(item.getDrinkerName().toString());
			holder.date.setText("Created on: " + item.getCreateOn().toString());
			holder.method.setText(item.getCaseMethod().toString());
			holder.val.setText(item.getBacValue());

			holder.method.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Result fragment = Result.create(item.getDrinkerName(),
							splitBac(item.getBacValue()));
					((BaseContainerFragment) getParentFragment())
							.replaceFragment(fragment, true);
				}
			});
			holder.delete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							getActivity());

					// set title
					alertDialogBuilder.setTitle("Alcohol Toxicology");

					// set dialog message
					alertDialogBuilder
							.setMessage("Click yes to delete")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is clicked, close
											// current activity
											deleteEntryFromList(item);
										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {

											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}

			});

			return convertView;
		}
	}

	private String splitBac(String weight) {
		String[] str = weight.split(" ");
		return str[0];
	}

	protected void deleteEntryFromList(CaseProfileModal item) {

		new DeleteEntry(item).execute();

	}

	class ViewHolder {
		TextView name;
		TextView date;
		TextView method;
		TextView val;
		ImageView delete;
	}

	public void UpdateUi(List<CaseProfileModal> result) {
		if (result != null && result.size() != 0) {
			list.setVisibility(View.VISIBLE);
			textNoInfo.setVisibility(View.GONE);
			listItems = result;
			adapter.updateItem(result);
		} else {
			list.setVisibility(View.GONE);
			textNoInfo.setVisibility(View.VISIBLE);
		}

	}

	class DeleteEntry extends AsyncTask<Object, Object, Boolean> {
		String TAG = "GetDrinkAsync Task";
		DbOperation db;
		CaseProfileModal item;

		public DeleteEntry(CaseProfileModal item) {
			this.db = new DbOperation(getActivity());
			this.item = item;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			boolean status = false;
			try {
				status = db.deleteEntry(item.getCaseId());
			} catch (SQLException e) {
				Log.e(TAG, e.getStackTrace() + "");
			}

			return status;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			if (result) { 
				listItems.remove(item);
				adapter.updateItem(listItems);
			} else {
				LogMsg.LOG(getActivity(), "Sry unable to delete record");
			}

		}
	}

}
