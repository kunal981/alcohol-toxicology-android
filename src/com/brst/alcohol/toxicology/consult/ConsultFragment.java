package com.brst.alcohol.toxicology.consult;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.brst.alcohol.toxicology.R;
import com.brst.alcohol.toxicology.util.LogMsg;

public class ConsultFragment extends Fragment implements OnClickListener {

	EditText name, message;
	Button send, cancel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.fragment_layout_consult, null);

		name = (EditText) rootView.findViewById(R.id.name);
		message = (EditText) rootView.findViewById(R.id.message);
		send = (Button) rootView.findViewById(R.id.send);
		cancel = (Button) rootView.findViewById(R.id.cancel);

		send.setOnClickListener(this);
		cancel.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		switch (id) {
		case R.id.send:

			String strName = name.getText().toString().trim();
			String strMessage = message.getText().toString().trim();
			if (strName != null && !strName.equals("") && strMessage != null
					&& !strMessage.equals("")) {

				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(
						Intent.EXTRA_EMAIL,
						new String[] { getString(R.string.text_feedback_email) });
				// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
				// email.putExtra(Intent.EXTRA_BCC, new String[]{to});
				email.putExtra(Intent.EXTRA_SUBJECT,
						"Alcohol Toxicology User Feedback");

				email.putExtra(Intent.EXTRA_TEXT, strMessage);

				// need this to prompts email client only
				email.setType("message/rfc822");

				startActivity(Intent.createChooser(email,
						"Choose an Email client :"));

			} else {
				LogMsg.LOG(getActivity(), "Enter detail first");
			}

			break;
		case R.id.cancel:
			name.setText("");
			name.setHint(getString(R.string.text_feedback_name_hint));
			message.setText("");
			message.setHint(getString(R.string.text_feedback_message_hint));

			break;

		default:
			break;
		}

	}
}
