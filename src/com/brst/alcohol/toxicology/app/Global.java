package com.brst.alcohol.toxicology.app;

import android.app.Application;

public class Global extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		initSingletons();
	}

	protected void initSingletons() {
		CaseDataAdapter.initInstance();
	}

}